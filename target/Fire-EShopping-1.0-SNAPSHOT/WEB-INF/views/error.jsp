<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Error</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>
	<div class="container">
		<h2>Oops! This is embarrassing :(</h2>
		<h4 class="text-danger">Either the page you are looking for was not found or an unexpected error occurred</h4>
	</div>
</body>
</html>