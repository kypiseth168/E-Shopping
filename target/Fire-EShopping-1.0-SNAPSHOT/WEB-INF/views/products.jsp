<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Products</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>
	<div class="container">
		<sec:authorize url="/products/add">
			<div class="row">
				<div class=" col-md-2 col-md-offset-10 ">
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#addProduct">Add Product</button>
				</div>
			</div>
			<hr />
			<div id="addProduct" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Product</h4>
						</div>
						<div class="modal-body">

							<form action="<c:url value="products/add"/>" method="POST"
								class="form-horizontal">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<div class="form-group">
									<label class="col-md-4 control-label">Product code:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="code"
											placeholder="Product Code" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Product name:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="name"
											placeholder="Product Name" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Product
										description:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="description"
											placeholder="Product Description" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Product price:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="price"
											placeholder="Product Price" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Category:</label>
									<div class="col-md-4">
										<select name="categoryId" class="form-control">
											<c:forEach var="category" items="${categoryList}">
												<option value="${category.id}">${category.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-8 col-md-offset-4">
										<input type="submit" class="btn btn-default" value="Save" />
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>
			<br />
			<br />
		</sec:authorize>

		<sec:authorize url="/products">
			<div class="well">
				<form class="form-horizontal" id="product-search-form"
					action="<c:url value="/products"/>" method="get">
					<fieldset>
						<legend>Search products</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name</label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="product_name"
									placeholder="Name" value="${searchProductName}">
							</div>
						</div>
						<div class="form-group">
							<label for="select" class="col-md-3 control-label">Category</label>
							<div class="col-md-9">
								<select class="form-control" name="category_id">
									<option value="-1">All</option>
									<c:forEach var="category" items="${categoryList}">
										<option value="${category.id}"
											<c:if test="${category.id == searchCategoryId}">selected=selected</c:if>>${category.name}</option> >${category.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-10 col-md-offset-3">
								<button type="submit" class="btn btn-primary">Search</button>
								<button type="reset" class="btn btn-danger"
									onclick="$('#product-search-form').trigger('reset');">Clear</button>
							</div>
						</div>
					</fieldset>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</div>
		</sec:authorize>

		<c:forEach var="product" items="${productList}">
			<div class="well">
				<div class="row">
					<div class="col-md-9">
						<h4>${product.category.name}</h4>
						<h4>${product.code} - ${product.name}</h4>

						<h5>${product.description}</h5>
						<h4>
							<b>Price: </b> $ ${product.price}
						</h4>
					</div>
					<div class="col-md-3">
						<sec:authorize url="/products/addtocart/${product.id}">
							<div class="row">
								<form action="<c:url value="products/addtocart/${product.id}"/>"
									method="POST" class="form-horizontal">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
									<div class="form-group">
										<div class="col-md-6">
											<input class="form-control" type="number" name="quantity"
												placeholder="quantity" />
										</div>
										<div class="col-md-6">
											<input type="submit" class="btn btn-success"
												value="Add to cart" />
										</div>
									</div>
								</form>
							</div>
						</sec:authorize>
						<div class="row">
							<sec:authorize url="/products/edit/1">
								<button type="button" class="btn btn-primary col-md-4"
									data-toggle="modal" data-target="#editProduct${product.id}">Edit</button>
								<div id="editProduct${product.id}" class="modal fade"
									role="dialog">
									<div class="modal-dialog">
										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit Product</h4>
											</div>
											<div class="modal-body">

												<form action="<c:url value="products/edit/${product.id}"/>"
													method="POST" class="form-horizontal">
													<input type="hidden" name="${_csrf.parameterName}"
														value="${_csrf.token}" />
													<div class="form-group">
														<label class="col-md-4 control-label">Product
															code:</label>
														<div class="col-md-4">
															<input type="text" class="form-control" name="code"
																placeholder="Product Code" value="${product.code}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Product
															name:</label>
														<div class="col-md-4">
															<input type="text" class="form-control" name="name"
																placeholder="Product Name" value="${product.name}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Product
															description:</label>
														<div class="col-md-4">
															<input type="text" class="form-control"
																name="description" placeholder="Product Description"
																value="${product.description}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Product
															price:</label>
														<div class="col-md-4">
															<input type="text" class="form-control" name="price"
																placeholder="Product Price" value="${product.price}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Category:</label>
														<div class="col-md-4">
															<select name="categoryId" class="form-control">
																<c:forEach var="category" items="${categoryList}">
																	<option value="${category.id}"
																		<c:if test="${category.id == product.category.id}">selected=selected</c:if>>${category.name}</option>
																</c:forEach>
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-8 col-md-offset-4">
															<input type="submit" class="btn btn-default" value="Save" />
														</div>
													</div>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default"
													data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>
								</div>
							</sec:authorize>
							<div class="col-md-2"></div>
							<sec:authorize url="/products/delete/{product.id}">
								<form action="<c:url value="products/delete/${product.id}"/>"
									class="form-horizontal" method="POST"
									onsubmit="return confirm('Do you really want to delete this product?');">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" /> <input type="submit"
										class="btn btn-danger col-md-4" value="Delete" />
								</form>
							</sec:authorize>
							<div class="col-md-2"></div>

						</div>
					</div>

				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>