<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Login</title>
<%@include file="partial_scripts.jsp"%>
</head>
<body>
	<%@include file="partial_navbar.jsp"%>

	<div class="well" style="max-width: 500px; margin: 0 auto">
		<fieldset>

			<form class="form-horizontal" id="login-form"
				action="<c:url value="/postLogin"/>" method="post">
				<legend>Login</legend>
				<div class="col-md-9 col-md-offset-3">
					<div class="text-danger">${error}</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label required" for="username">Username
						<span class="required">*</span>
					</label>
					<div class="col-md-9">
						<input class="form-control" placeholder="Username" name="username"
							type="text"
							value='<c:if test="${not empty param.login_error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>'>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label required" for="password">Password
						<span class="required">*</span>
					</label>
					<div class="col-md-9">
						<input class="form-control" placeholder="Password" name="password"
							type="password" />
					</div>
				</div>
				<div class="col-md-9 col-md-offset-3">
					<input class="btn btn-primary" type="submit" name="yt0"
						value="Login"> <input class="btn btn-danger btnClear"
						name="yt1" type="button" value="Clear" onclick="$('#login-form').trigger('reset');">
				</div>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		</fieldset>
	</div>

</body>
</html>