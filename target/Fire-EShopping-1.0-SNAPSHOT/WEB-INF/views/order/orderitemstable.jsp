

<table class="table table-hover">
	<thead>
		<tr>

			<th>Code</th>
			<th>Product</th>
			<th>Category</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${orderitems.size()==0}">
				<tr class="danger">
					<td colspan="3">
						<p class="text-center">
							<strong> Nothing to display... sorry! </strong>
						</p>
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach items="${orderitems}" var="orderitem">
					<tr>
						<td>${orderitem.productHistory.code }</td>
						<td>${orderitem.productHistory.name }<br />
							${orderitem.productHistory.description }
						</td>
						<td>${orderitem.productHistory.category }</td>
						<td>${orderitem.quantity }</td>
						<td>${orderitem.productHistory.price }</td>
						<td>${orderitem.amount }</td>
					</tr>
				</c:forEach>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th>Sub Total<br />Tax<br />Total</th>
					<th>${order.subtotal}<br />${order.tax}<br />${order.total}</th>
				</tr>

			</c:otherwise>
		</c:choose>
	</tbody>
</table>
