<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Profile</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>
	<div class="container">
		<div class="well bs-component">
			<form class="form-horizontal" method="post"
				action="<c:url value="/customer/updateprofile"/>">
				<fieldset>
					<legend>View Profile : ${username}</legend>
					<div class="form-group">
						<label class="col-md-3 control-label">Firstname</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="firstname"
								placeholder="Firstname" value="${customer.firstname}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Lastname</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="lastname"
								placeholder="Lastname" value="${customer.lastname}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Street</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="street"
								placeholder="Street" value="${customer.address.street}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">City</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="city"
								placeholder="City" value="${customer.address.city}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">State</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="state"
								placeholder="State" value="${customer.address.state}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Zip</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="zip"
								placeholder="Zip" value="${customer.address.zip}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-9 col-md-offset-3">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</fieldset>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		</div>
	</div>
</body>
</html>