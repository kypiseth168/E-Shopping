
<div class="navbar-inverse navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a href="<c:url value="/"/>" class="navbar-brand">Fire E-Shopping</a>
		</div>
		<div class="navbar-collapse collapse" id="navbar-main">
			<ul class="nav navbar-nav">
				<sec:authorize url="/categories">
					<li><a href="<c:url value="/categories"/>">Categories</a></li>
				</sec:authorize>
				<sec:authorize url="/products">
					<li><a href="<c:url value="/products"/>">Products</a></li>
				</sec:authorize>
				<sec:authorize url="/cart">
					<li><a href="<c:url value="/cart"/>">Cart</a></li>
				</sec:authorize>
				<sec:authorize url="/orders">
					<li><a href="<c:url value="/orders"/>">Orders</a></li>
				</sec:authorize>
				<sec:authorize url="/customer/orders">
					<li><a href="<c:url value="/customer/orders"/>">Orders</a></li>
				</sec:authorize>
			</ul>

			<sec:authorize url="/products">
				<sec:authentication var="user" property="principal" />

				<form id="logout-form" action="<c:url value="/doLogout"/>"
					method="post">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#" id="user-options">Hello
								${user.username} <span class="caret"></span>
						</a>
							<ul class="dropdown-menu" aria-labelledby="user-options">
								<sec:authorize url="/customer/profile">
									<li><a href="<c:url value="/customer/profile"/>">Profile</a></li>
								</sec:authorize>
								<li><a href="javascript:{}"
									onclick="document.getElementById('logout-form').submit(); return false;">Logout</a></li>
							</ul></li>
					</ul>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</sec:authorize>
		</div>
	</div>
</div>

<br />
<br />
<br />
<br />
<script type="text/javascript">
<%if (request.getSession().getAttribute("successMessage") != null) {%>
	BootstrapDialog.show({
		message : "<%out.print(request.getSession().getAttribute("successMessage"));%>",
		type : BootstrapDialog.TYPE_SUCCESS
	});
<%request.getSession().removeAttribute("successMessage");
			}%>
<%if (request.getSession().getAttribute("errorMessage") != null) {%>
BootstrapDialog.show({
	message : "<%out.print(request.getSession().getAttribute("errorMessage"));%>
	",
				type : BootstrapDialog.TYPE_ERROR
			});
<%request.getSession().removeAttribute("errorMessage");
			}%>
	
</script>