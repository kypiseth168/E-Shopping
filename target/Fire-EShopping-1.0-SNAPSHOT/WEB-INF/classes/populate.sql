-- add logins for some users
INSERT INTO user(id,username,password,role,is_active) VALUES (1,'admin','admin', 'ROLE_ADMIN',TRUE);
INSERT INTO user(id,username,password,role,is_active) VALUES (2,'user','user', 'ROLE_USER',TRUE);
INSERT INTO user(id,username,password,role,is_active) VALUES (3,'ashraf','user', 'ROLE_USER',TRUE);
INSERT INTO user(id,username,password,role,is_active) VALUES (4,'hiten','user', 'ROLE_USER',TRUE);
INSERT INTO user(id,username,password,role,is_active) VALUES (5,'sardar','user', 'ROLE_USER',TRUE);


-- add some customers
INSERT INTO customer(id,city,state,street,zip,firstname,lastname,is_active,user_id) values(1,'Fairfield','Iowa','North Fourth Street','52557','Tonghann','Teng',TRUE,2);
INSERT INTO customer(id,city,state,street,zip,firstname,lastname,is_active,user_id) values(2,'Fairfield','Iowa','North Fourth Street','52557','Ashraf','Ezzat',TRUE,3);
INSERT INTO customer(id,city,state,street,zip,firstname,lastname,is_active,user_id) values(3,'Fairfield','Iowa','North Fourth Street','52557','Hiten','Vaint',TRUE,4);
INSERT INTO customer(id,city,state,street,zip,firstname,lastname,is_active,user_id) values(4,'Fairfield','Iowa','North Fourth Street','52557','Sardar','Wali',TRUE,5);

-- add some catgories
INSERT INTO category (id, is_active, category_name) VALUES (1, true, 'Computers');
INSERT INTO category (id, is_active, category_name) VALUES (2, true, 'Clothing');
INSERT INTO category (id, is_active, category_name) VALUES (3, true, 'Toys');
INSERT INTO category (id, is_active, category_name) VALUES (4, true, 'Cars');

-- add some products

-- computers category
INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('1', '0001-00001', 'ASUS mainstream tower Black M32CD-AS31', 'ASUS M32CD-AS31 (6th Generation Core i3, 8GB DDR4, 1TB HDD, Windows 10)', '374.99', '1',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('1', 'Computers', '0001-00001', 'ASUS mainstream tower Black M32CD-AS31', 'ASUS M32CD-AS31 (6th Generation Core i3, 8GB DDR4, 1TB HDD, Windows 10)', '374.99', '1');

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('2', '0001-00002', 'Dell OptiPlex 960 SFF Desktop PC', 'Everyday computing just got easier with the Dell OptiPlex 960 SFF Desktop PC.', '135.65', '1',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('2', 'Computers', '0001-00002', 'Dell OptiPlex 960 SFF Desktop PC', 'Everyday computing just got easier with the Dell OptiPlex 960 SFF Desktop PC.', '135.65', '2');

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('3', '0001-00003', 'Brother HL-L2300D Monochrome Laser Printer with Duplex Printing', 'Reliable Affordable Monochrome Laser Printer For Personal or Home Office Use.', '84.99', '1',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('3', 'Computers', '0001-00003', 'Brother HL-L2300D Monochrome Laser Printer with Duplex Printing', 'Reliable Affordable Monochrome Laser Printer For Personal or Home Office Use.', '84.99', '3');

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('4', '0001-00004', 'Kinobo USB Microphone "Kanji" For Chat, Skype Microphone', '"Kanji" is the newest version of our popular USB Microphone. This USB Microphone with excellent audio clarity', '13.99', '1',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('4', 'Computers', '0001-00004', 'Kinobo USB Microphone "Kanji" For Chat, Skype Microphone', '"Kanji" is the newest version of our popular USB Microphone. This USB Microphone with excellent audio clarity', '13.99', '4');


-- Clothes Category

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('5', '0002-00001', 'Champion Men Jersey T-Shirt', 'ASUS M32CD-AS31 (6th Generation Core i3, 8GB DDR4, 1TB HDD, Windows 10)', '3.90', '2',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('1', 'Clothing', '0002-00001', 'Champion Men Jersey T-Shirt', 'The Champion Jersey Tee - the ultimate in comfort and durability.', '3.90', '5');

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('6', '0002-00002', 'Baby World Kids Girls Harness Heart-shaped', 'Baby World Kids Girls Harness Heart-shaped Rompers Summer Jumpsuit Clothes', '13.99', '2',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('6', 'Clothing', '0002-00002', 'Baby World Kids Girls Harness Heart-shaped', 'Baby World Kids Girls Harness Heart-shaped Rompers Summer Jumpsuit Clothes', '13.99', '6');

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('7', '0002-00003', 'Russell Athletic Men Short-Sleeve Cotton T-Shirt', 'Russell Athletic Men Short-Sleeve Cotton T-Shirt', '7.99', '2',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('7', 'Clothing', '0002-00003', 'Russell Athletic Men Short-Sleeve Cotton T-Shirt', 'Russell Athletic Men Short-Sleeve Cotton T-Shirt', '7.99', '7');

INSERT INTO product (`id`, `product_code`, `product_description`, `product_name`, `product_price`, `category_id`,`is_active`) VALUES ('8', '0002-00004', 'Kids Girls Comic Graffiti Leopard', 'SELFIE #HASHTAG Print Party Cropped Top&Plain BlackSkater Skirt Set.', '19.99', '2',true);
INSERT INTO producthistory (`id`, `category`, `product_code`,  `product_name`,`product_description`, `product_price`, `product_id`) VALUES ('8', 'Clothing', '0002-00004', 'Kids Girls Comic Graffiti Leopard', 'SELFIE #HASHTAG Print Party Cropped Top&Plain BlackSkater Skirt Set.', '19.99', '8');




















