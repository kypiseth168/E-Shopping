<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Orders</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>


	<div class="container">
		<h2>List of orders</h2>
		<hr />
		<table class="table table-hover">
			<thead>
				<tr>

					<th>Order number</th>
					<th>Order date</th>
					<sec:authorize url="/orders/${order.id}">
						<th>Customer</th>
					</sec:authorize>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${orders}" var="order">
					<tr>
						<td>${order.number }</td>
						<td>${order.date }</td>
						<sec:authorize url="/orders/${order.id}">
							<td>${order.customer.firstname }</td>
						</sec:authorize>
						<td><sec:authorize url="/customer/orders/${order.id }">
								<a class="btn btn-primary btn-sm"
									href="<c:url value="/customer/orders/${order.id}"/>">Details</a>
							</sec:authorize> <sec:authorize url="/orders/${order.id}">
								<a class="btn btn-primary btn-sm"
									href="<c:url value="/orders/${order.id}"/>">Details</a>
							</sec:authorize></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>