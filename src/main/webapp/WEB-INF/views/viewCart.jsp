<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Cart</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>
	<div class="container">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Code</th>
					<th>Product</th>
					<th>Category</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${orderList}" var="item">

					<tr>
						<td>${item.productHistory.code}</td>
						<td>${item.productHistory.name }<br/>${item.productHistory.description }</td>
						<td>${item.productHistory.category }</td>
						<td>${item.quantity }</td>
						<td>${item.productHistory.price }</td>
						<td>${item.amount}</td>
						<td>
							<button type="button" class="btn btn-primary btn-sm"
								data-toggle="modal" data-target="#editCart${item.productHistory.product.id}">Edit</button>
							<div id="editCart${item.productHistory.product.id}"
								class="modal fade" role="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Edit cart item</h4>
										</div>
										<div class="modal-body">

											<form
												action="<c:url value="cart/edit/${item.productHistory.product.id}"/>"
												method="POST" class="form-horizontal">
												<input type="hidden" name="${_csrf.parameterName}"
													value="${_csrf.token}" />
												<div class="form-group">
													<label for="categoryName" class="col-md-4 control-label">Quantity:</label>
													<div class="col-md-4">
														<input type="number" class="form-control" name="quantity"
															placeholder="Quantity" value="${item.quantity}" />
													</div>
													<div class="col-md-4">
														<input type="submit" class="btn btn-default"
															value="Update" />
													</div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>

						</td>
						<td>
							<form
								action="<c:url value="cart/delete/${item.productHistory.product.id}"/>"
								method="POST"
								onsubmit="return confirm('Are you sure you want to delete this item from the cart?');"
								class="form-horizontal">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" /> <input type="submit"
									class="btn btn-danger btn-sm" value="Delete" />
							</form>
						</td>
					</tr>
				</c:forEach>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th>Sub Total<br/>Tax<br/>Total</th>
					<th>${subTotal}<br/>${tax}<br/>${total}</th>
				</tr>
			</tbody>
		</table>
		<form class="form-horizontal" method="post"
			action="<c:url value="cart/checkout"/>">
			<fieldset>
				<legend>Shipping Address</legend>

				<div class="form-group">
					<label class="col-md-3 control-label">Street</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="street"
							placeholder="Street" value="${customer.address.street}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">City</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="city"
							placeholder="City" value="${customer.address.city}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">State</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="state"
							placeholder="State" value="${customer.address.state}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Zip</label>
					<div class="col-md-9">
						<input type="text" class="form-control" name="zip"
							placeholder="Zip" value="${customer.address.zip}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<button type="submit" class="btn btn-primary">Checkout</button>
					</div>
				</div>
			</fieldset>
			 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>




</body>
</html>