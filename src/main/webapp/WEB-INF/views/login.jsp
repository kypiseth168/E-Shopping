<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Login</title>
<%@include file="partial_scripts.jsp"%>
</head>
<body>
	<%@include file="partial_navbar.jsp"%>

	<div class="well" style="max-width: 500px; margin: 0 auto">
		<fieldset>

			<form class="form-horizontal" id="login-form"
				action="<c:url value="/postLogin"/>" method="post">
				<legend>Login</legend>
				<div class="col-md-9 col-md-offset-3">
					<div class="text-danger">${error}</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label required" for="username">Username
						<span class="required">*</span>
					</label>
					<div class="col-md-9">
						<input class="form-control" placeholder="Username" name="username"
							type="text"
							value='<c:if test="${not empty param.login_error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>'>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label required" for="password">Password
						<span class="required">*</span>
					</label>
					<div class="col-md-9">
						<input class="form-control" placeholder="Password" name="password"
							type="password" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<input class="btn btn-primary" type="submit" name="yt0"
							value="Login"> <input class="btn btn-danger btnClear"
							name="yt1" type="button" value="Clear"
							onclick="$('#login-form').trigger('reset');">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<button type="button" class="btn btn-primary" data-toggle="modal"
							data-target="#addProduct">Sign up</button>
					</div>
				</div>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		</fieldset>
	</div>
	<!--  New code  -->

	<div id="addProduct" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Sign up</h4>
				</div>
				<div class="modal-body">

					<form action="<c:url value="/signup"/>" method="POST"
						class="form-horizontal">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="form-group">
							<label class="col-md-4 control-label">Firstname:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="firstname"
									placeholder="Firstname" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Lastname:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="lastname"
									placeholder="Lastname" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Username:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="username"
									placeholder="Username" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Password:</label>
							<div class="col-md-4">
								<input type="password" class="form-control" name="password"
									placeholder="Password" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Street</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="street"
									placeholder="Street" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">City:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="city"
									placeholder="city" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">State:</label>

							<div class="col-md-4">
								<input type="text" class="form-control" name="state"
									placeholder="State" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Zip:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="zip"
									placeholder="Zip code" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<input type="submit" class="btn btn-primary" value="Sign up" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>