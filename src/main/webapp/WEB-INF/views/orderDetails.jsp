<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Order Details</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>


	<div class="container">
		<h2>Order details</h2>
		<hr />
		<div class="row">

			<div class="col-md-4">

				<address>
					<strong>Shipping Address</strong><br> ${ shipingAddress.street }<br>
					${ shipingAddress.city }, ${ shipingAddress.state } ${ shipingAddress.zip }<br>
				</address>

			</div>


			<div class="col-md-4">

				<strong>Order : </strong>${order.number } <br> <Strong>Date
					:</Strong> ${order.date }
			</div>

			<div class="col-md-4">
				<strong>${customer.firstname}</strong><br>

			</div>
			<br />

		</div>
		<div class="row">
			<div class="col-md-12">
				<%@include file="order/orderitemstable.jsp"%>
			</div>
		</div>
	</div>
</body>
</html>