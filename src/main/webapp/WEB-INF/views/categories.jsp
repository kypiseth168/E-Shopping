<%@include file="partial_taglibs.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fire E-Shopping - Categories</title>
<%@include file="partial_scripts.jsp"%>

</head>
<body>
	<%@include file="partial_navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class=" col-md-2 col-md-offset-10 ">
				<button type="button" class="btn btn-primary" data-toggle="modal"
					data-target="#addCategory">Add Category</button>
			</div>
		</div>
		<hr />
		<div id="addCategory" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Category</h4>
					</div>
					<div class="modal-body">

						<form action="<c:url value="categories/add"/>" method="POST"
							class="form-horizontal">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<div class="form-group">
								<label for="categoryName" class="col-md-4 control-label">Category
									name:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="name"
										placeholder="Category name" />
								</div>
								<div class="col-md-4">
									<input type="submit" class="btn btn-default" value="Save" />
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>



		<br />
		<h3>Category list</h3>






		<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="category" items="${categoryList}">
					<tr>
						<td><h4>${category.name}</h4></td>
						<td>
							<div class="row">
								<div class="col-md-2">
									<button type="button" class="btn btn-primary btn-sm"
										data-toggle="modal" data-target="#editCategory${category.id}">Edit</button>
									<div id="editCategory${category.id}" class="modal fade"
										role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Edit Category</h4>
												</div>
												<div class="modal-body">

													<form
														action="<c:url value="categories/edit/${category.id}"/>"
														method="POST" class="form-horizontal">
														<input type="hidden" name="${_csrf.parameterName}"
															value="${_csrf.token}" />
														<div class="form-group">
															<label for="categoryName" class="col-md-4 control-label">Category
																name:</label>
															<div class="col-md-4">
																<input type="text" class="form-control" name="name"
																	placeholder="Category name" value="${category.name}" />
															</div>
															<div class="col-md-4">
																<input type="submit" class="btn btn-default"
																	value="Update" />
															</div>
														</div>
														<div class="modal-footer">
															<label></label>
														</div>
													</form>
												</div>

												<div class="modal-footer">
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<form
										action="<c:url value="categories/delete/${category.id}"/>"
										method="POST"
										onsubmit="return confirm('Do you really want to delete this category?');">
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit"
											class="btn btn-danger btn-sm" value="Delete" />
									</form>
								</div>
							</div>
						</td>
				</c:forEach>






			</tbody>
		</table>
</body>
</html>