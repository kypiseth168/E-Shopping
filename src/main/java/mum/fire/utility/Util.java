package mum.fire.utility;

public final class Util {

	private Util() {
		//to make sure class is singleton 
	}

	public static final String SUCCESS_MESSAGE = "successMessage";
	public static final String ERROR_MESSAGE = "errorMessage";

	public static final String getOrderItemSession(String username) {
		return "orderItems_" + username;
	}

	public static final double getTax(double subTotal) {
		return subTotal * 1.07;
	}

}
