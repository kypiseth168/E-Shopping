package mum.fire.repository;

import java.util.List;

import mum.fire.repository.query.QuerySpecification;

public interface BaseRepository<T> {

	T add(T t);

	T update(T t);

	boolean delete(long id);

	T get(long id);

	T get(QuerySpecification querySpecification);

	List<T> getList(QuerySpecification querySpecification);

}
