package mum.fire.repository;

import mum.fire.domain.Category;

public interface CategoryRepository extends BaseRepository<Category> {

}
