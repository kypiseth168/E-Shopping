package mum.fire.repository;

import mum.fire.domain.Order;

public interface OrderRepository extends BaseRepository<Order> {

}
