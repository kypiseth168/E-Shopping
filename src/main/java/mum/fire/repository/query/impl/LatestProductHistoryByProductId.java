package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public class LatestProductHistoryByProductId implements QuerySpecification {

	private long id;

	public LatestProductHistoryByProductId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getQueryString() {
		return "SELECT ph from ProductHistory ph where ph.product.id =:productId Order by id desc";
	}

	@Override
	public void setQueryParameters(Query query) {
		query.setLong("productId", id);
	}

}
