package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public enum AllProducts implements QuerySpecification {

	INSTANCE;
	@Override
	public String getQueryString() {
		return "SELECT p from Product p where p.isActive=true";
	}

	@Override
	public void setQueryParameters(Query query) {
		
	}

}
