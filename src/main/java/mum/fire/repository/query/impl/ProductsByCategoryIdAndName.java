package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public class ProductsByCategoryIdAndName implements QuerySpecification {

	private long categoryId;
	private String name;

	public ProductsByCategoryIdAndName(long categoryId, String name) {
		this.categoryId = categoryId;
		this.name = name;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getQueryString() {
		String query = "";
		name = name == null ? "" : name;
		String criteria = "";
		if (categoryId != -1) {
			criteria += "p.category.id = :categoryId";
		} else {
			criteria += "p.category.id <> :categoryId";
		}
		criteria += " and (p.name like :productName or p.description like :productName) and p.isActive=true";
		query = "SELECT p from Product p where " + criteria;
		return query;
	}

	@Override
	public void setQueryParameters(Query query) {
		query.setLong("categoryId", categoryId);
		query.setString("productName", "%" + name + "%");
	}

}
