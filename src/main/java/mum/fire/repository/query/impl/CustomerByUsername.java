package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public class CustomerByUsername implements QuerySpecification {

	private String username;

	public CustomerByUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getQueryString() {
		return "SELECT c from Customer c where c.user.username=:username";
	}

	@Override
	public void setQueryParameters(Query query) {
		query.setString("username", username);
	}

}
