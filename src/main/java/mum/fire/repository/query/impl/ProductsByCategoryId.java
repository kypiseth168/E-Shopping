package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public class ProductsByCategoryId implements QuerySpecification {

	private long id;

	public ProductsByCategoryId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getQueryString() {
		return "SELECT p from Product p where p.isActive=true and p.category.id = :id";
	}

	@Override
	public void setQueryParameters(Query query) {
		query.setLong("id", id);
	}

}
