package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public enum AllOrders implements QuerySpecification {

	INSTANCE;
	@Override
	public String getQueryString() {
		return "SELECT i from Order i";
	}

	@Override
	public void setQueryParameters(Query query) {
		
	}

}
