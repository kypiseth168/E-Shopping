package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public enum AllProductHistory implements QuerySpecification {

	INSTANCE;
	@Override
	public String getQueryString() {
		return "SELECT ph from ProductHistory ph";
	}

	@Override
	public void setQueryParameters(Query query) {
		
	}

}
