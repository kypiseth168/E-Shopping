package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public class OrdersByCustomerId implements QuerySpecification {

	private long id;

	public OrdersByCustomerId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getQueryString() {
		return "SELECT i from Order i where i.customer.id=:id";
	}

	@Override
	public void setQueryParameters(Query query) {
		query.setLong("id", id);
	}

}
