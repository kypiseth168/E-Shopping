package mum.fire.repository.query.impl;

import org.hibernate.Query;

import mum.fire.repository.query.QuerySpecification;

public enum AllCustomers implements QuerySpecification {

	INSTANCE;
	@Override
	public String getQueryString() {
		return "SELECT c from Customer c where c.isActive=true";
	}

	@Override
	public void setQueryParameters(Query query) {
		
	}

}
