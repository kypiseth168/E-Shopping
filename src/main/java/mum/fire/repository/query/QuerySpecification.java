package mum.fire.repository.query;

import org.hibernate.Query;

public interface QuerySpecification {

	public String getQueryString();

	public void setQueryParameters(Query query);

}
