package mum.fire.repository;

import mum.fire.domain.Product;

public interface ProductRepository extends BaseRepository<Product> {

}
