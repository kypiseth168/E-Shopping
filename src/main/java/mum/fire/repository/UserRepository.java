package mum.fire.repository;

import mum.fire.domain.User;

public interface UserRepository extends BaseRepository<User> {
	
}
