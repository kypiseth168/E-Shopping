package mum.fire.repository;

import mum.fire.domain.ProductHistory;

public interface ProductHistoryRepository extends BaseRepository<ProductHistory> {

}
