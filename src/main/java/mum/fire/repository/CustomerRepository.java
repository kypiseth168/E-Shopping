package mum.fire.repository;

import mum.fire.domain.Customer;

public interface CustomerRepository extends BaseRepository<Customer> {
	
}
