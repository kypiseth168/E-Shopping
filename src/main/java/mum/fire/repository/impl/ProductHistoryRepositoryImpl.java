package mum.fire.repository.impl;

import org.springframework.stereotype.Repository;

import mum.fire.domain.ProductHistory;
import mum.fire.repository.ProductHistoryRepository;

@Repository
public class ProductHistoryRepositoryImpl extends BaseRepositoryImpl<ProductHistory>
		implements ProductHistoryRepository {
}
