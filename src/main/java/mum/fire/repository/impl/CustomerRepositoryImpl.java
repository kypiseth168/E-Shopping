package mum.fire.repository.impl;

import org.springframework.stereotype.Repository;

import mum.fire.domain.Customer;
import mum.fire.repository.CustomerRepository;

@Repository
public class CustomerRepositoryImpl extends BaseRepositoryImpl<Customer> implements CustomerRepository {

	@Override
	public Customer add(Customer t) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().persist(t);
		return t;
	}

}
