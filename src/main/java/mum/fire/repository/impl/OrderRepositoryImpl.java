package mum.fire.repository.impl;

import org.springframework.stereotype.Repository;

import mum.fire.domain.Order;
import mum.fire.repository.OrderRepository;

@Repository
public class OrderRepositoryImpl extends BaseRepositoryImpl<Order> implements OrderRepository {

	@Override
	public Order add(Order t) {
		sessionFactory.getCurrentSession().persist(t);
		return t;
	}
}