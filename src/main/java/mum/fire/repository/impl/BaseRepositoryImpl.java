package mum.fire.repository.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import mum.fire.repository.BaseRepository;
import mum.fire.repository.query.QuerySpecification;

public abstract class BaseRepositoryImpl<T> implements BaseRepository<T> {

	@Autowired
	protected SessionFactory sessionFactory;

	private Class<T> type;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BaseRepositoryImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class) pt.getActualTypeArguments()[0];
	}

	public T add(T t) {
		sessionFactory.getCurrentSession().save(t);
		return t;
	}

	public T update(T t) {
		sessionFactory.getCurrentSession().saveOrUpdate(t);
		return t;
	}

	public boolean delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		session.delete((T) session.load(type, id));
		return true;
	}

	public T get(long id) {
		return (T) sessionFactory.getCurrentSession().get(type, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(QuerySpecification querySpecification) {
		Query query = sessionFactory.getCurrentSession().createQuery(querySpecification.getQueryString());
		querySpecification.setQueryParameters(query);
		query.setMaxResults(1);
		List<T> resultList = (List<T>) query.list();
		for (T result : resultList) {
			return result;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getList(QuerySpecification querySpecification) {
		Query query = sessionFactory.getCurrentSession().createQuery(querySpecification.getQueryString());
		querySpecification.setQueryParameters(query);
		return (List<T>) query.list();
	}
}
