package mum.fire.repository.impl;

import org.springframework.stereotype.Repository;

import mum.fire.domain.Category;
import mum.fire.repository.CategoryRepository;

@Repository
public class CategoryRepositoryImpl extends BaseRepositoryImpl<Category> implements CategoryRepository {

}
