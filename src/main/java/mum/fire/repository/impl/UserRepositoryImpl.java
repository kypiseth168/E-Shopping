package mum.fire.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import mum.fire.domain.User;
import mum.fire.repository.UserRepository;

@Repository
public class UserRepositoryImpl extends BaseRepositoryImpl<User> implements UserRepository {

	@SuppressWarnings("unchecked")
	public List<User> getAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT u from User u where u.isActive=true");
		return new ArrayList<User>(query.list());
	}

}
