package mum.fire.repository.impl;

import org.springframework.stereotype.Repository;

import mum.fire.domain.Product;
import mum.fire.repository.ProductRepository;

@Repository
public class ProductRepositoryImpl extends BaseRepositoryImpl<Product> implements ProductRepository {

}
