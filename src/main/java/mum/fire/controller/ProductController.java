package mum.fire.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mum.fire.domain.Category;
import mum.fire.domain.OrderItem;
import mum.fire.domain.Product;
import mum.fire.domain.ProductHistory;
import mum.fire.service.CategoryService;
import mum.fire.service.ProductHistoryService;
import mum.fire.service.ProductService;
import mum.fire.utility.Util;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductHistoryService produceHistoryService;

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public String getAllProducts(Model model, HttpServletRequest request) {
		List<Product> products;
		String searchName = "";
		long searchCategoryId = -1;
		if (request.getParameter("category_id") != null && request.getParameter("product_name") != null) {
			searchName = request.getParameter("product_name").toString();
			searchCategoryId = Long.parseLong(request.getParameter("category_id"));
			products = productService.findProduct(searchCategoryId, searchName);
		} else {
			products = productService.getAllProducts();
		}
		List<Category> categories = categoryService.getAllCategories();
		model.addAttribute("categoryList", categories);
		model.addAttribute("productList", products);
		model.addAttribute("searchProductName", searchName);
		model.addAttribute("searchCategoryId", searchCategoryId);
		return "products";
	}

	@RequestMapping(value = "/products/category/{id}", method = RequestMethod.GET)
	public String getProductByCategory(@PathVariable Long id, Model model) {
		List<Product> products = productService.getProductByCategory(id);
		model.addAttribute("productList", products);
		return "products";
	}

	@RequestMapping(value = "/products/edit/{id}", method = RequestMethod.POST)
	public String updateProduct(Product updatedProduct, @PathVariable long id, HttpServletRequest request) {
		long categoryId = Long.parseLong(request.getParameter("categoryId"));
		Category category = categoryService.getCategory(categoryId);
		updatedProduct.setCategory(category);
		productService.updateProduct(updatedProduct, id);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Item successfully updated");
		return "redirect:/products";
	}

	@RequestMapping(value = "/products/add", method = RequestMethod.POST)
	public String addProduct(Product product, HttpServletRequest request) {
		long categoryId = Long.parseLong(request.getParameter("categoryId"));
		Category category = categoryService.getCategory(categoryId);
		product.setCategory(category);
		productService.addProduct(product);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Item successfully added");
		return "redirect:/products";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/products/addtocart/{id}", method = RequestMethod.POST)
	public String addToCart(@PathVariable long id, HttpServletRequest request, Principal principal) {
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		ProductHistory productHistory = produceHistoryService.getLastProductHistoryByProductId(id);
		String orderItemsSession = Util.getOrderItemSession(principal.getName());
		Map<Long, OrderItem> items;
		if (request.getSession().getAttribute(orderItemsSession) == null) {
			items = new HashMap<>();
		} else {
			items = (Map<Long, OrderItem>) request.getSession().getAttribute(orderItemsSession);
		}
		OrderItem orderItem = new OrderItem(quantity, productHistory);
		int existingQuantity = 0;
		if (items.containsKey(id)) {
			existingQuantity = items.get(id).getQuantity();
		}
		orderItem.setQuantity(orderItem.getQuantity() + existingQuantity);
		items.put(id, orderItem);
		request.getSession().setAttribute(orderItemsSession, items);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Item successfully added to cart");

		return "redirect:/products";
	}

	@RequestMapping(value = "/products/delete/{id}", method = RequestMethod.POST)
	public String deleteOrderItem(@PathVariable long id, HttpServletRequest request) {
		productService.deleteProduct(id);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Item successfully deleted");
		return "redirect:/products";
	}
}
