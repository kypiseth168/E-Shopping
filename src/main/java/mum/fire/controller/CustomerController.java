package mum.fire.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mum.fire.domain.Address;
import mum.fire.domain.Customer;
import mum.fire.domain.Order;
import mum.fire.domain.User;
import mum.fire.service.CustomerService;
import mum.fire.service.OrderService;
import mum.fire.utility.Util;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderController orderController;

	@RequestMapping("/customer/profile")
	public String viewProfile(Model model, Principal principal) {
		String username = principal.getName();
		Customer customer = customerService.getCustomerByUsername(username);
		model.addAttribute("customer", customer);
		model.addAttribute("username", username);
		return "profile";
	}

	@RequestMapping(value = "/customer/updateprofile", method = RequestMethod.POST)
	public String updateProfile(Customer postCustomer, Address postAddress, Principal principal,
			HttpServletRequest request) {
		String username = principal.getName();
		postCustomer.setAddress(postAddress);
		customerService.updateCustomer(postCustomer, username);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Profile successfully updated");
		return "redirect:/customer/profile";
	}

	@RequestMapping("/customer/orders")
	public String viewCustomerOrders(Model model, Principal principle) {
		Customer customer = customerService.getCustomerByUsername(principle.getName());
		List<Order> orders = orderService.getOrderByCustomerId(customer.getId());
		model.addAttribute("orders", orders);
		return "orders";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String customerSignUp(Customer customer, User user, Address address, HttpServletRequest request, Model model,
			Principal principle) {
		user.setActive(true);
		user.setRole("ROLE_USER");
		customer.setAddress(address);
		customer.setUser(user);
		customer.setActive(true);
		customerService.createCustomer(customer);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Sign up successfull");
		return "redirect:/login";
	}

	@RequestMapping("/customer/orders/{id}")
	public ModelAndView viewCustomerOrderDetails(@PathVariable long id, Principal principal) {
		Order order = orderService.getOrder(id);
		String username = order.getCustomer().getUser().getUsername();
		ModelAndView modelAndView = new ModelAndView();
		if (username.equals(principal.getName())) {
			modelAndView = orderController.orderDetails(id);
		} else {
			modelAndView.setViewName("redirect:/customer/orders");
		}
		return modelAndView;
	}

}
