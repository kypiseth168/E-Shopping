package mum.fire.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import mum.fire.domain.Order;
import mum.fire.service.OrderService;

@Controller
@RequestMapping(value = "/orders")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "")
	public String getOrders(Model model) {
		List<Order> orders = orderService.getAllOrders();
		model.addAttribute("orders", orders);
		return "orders";
	}

	@RequestMapping(value = "/{id}")
	public ModelAndView orderDetails(@PathVariable long id) {
		Order order = orderService.getOrder(id);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("order", order);
		modelAndView.addObject("orderitems", order.getItems());
		modelAndView.addObject("customer", order.getCustomer());
		modelAndView.addObject("shipingAddress", order.getShippingAddress());
		modelAndView.setViewName("orderDetails");
		return modelAndView;
	}

}
