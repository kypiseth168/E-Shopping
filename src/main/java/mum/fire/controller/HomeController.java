package mum.fire.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@ControllerAdvice
public class HomeController {

	@RequestMapping("/")
	public String rootContext() {
		return "index";
	}

	@RequestMapping("/login")
	public String login(HttpServletRequest request, Model model) {
		String errorMessage = "";
		if (request.getParameter("loginFailed") != null) {
			errorMessage = "Invalid login credentials";
		}
		model.addAttribute("error", errorMessage);
		return "login";
	}

	@ExceptionHandler(value = Exception.class)
	@RequestMapping("/error")
	public ModelAndView handle(Exception e) {
		ModelAndView mv = new ModelAndView();
		mv.getModel().put("e", e);
		mv.setViewName("error");
		return mv;
	}

}
