package mum.fire.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mum.fire.domain.Address;
import mum.fire.domain.Customer;
import mum.fire.domain.Order;
import mum.fire.domain.OrderItem;
import mum.fire.service.CustomerService;
import mum.fire.service.OrderService;
import mum.fire.utility.Util;

@Controller
public class CartController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private OrderService orderService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public String showCart(HttpServletRequest request, Model model, Principal principal) {

		String orderItemsSession = Util.getOrderItemSession(principal.getName());

		
		Collection<OrderItem> items = null;
		if (request.getSession().getAttribute(orderItemsSession) != null) {
			items = ((Map<String, OrderItem>) request.getSession().getAttribute(orderItemsSession)).values();
		}
		if (items == null || items.isEmpty()) {
			request.getSession().setAttribute(Util.ERROR_MESSAGE, "There are no items in your cart");
			return "redirect:/products";
		}
		double subTotal = 0;
		model.addAttribute("orderList", items);
		for (OrderItem item : items) {
			subTotal += item.getAmount();
		}
		Customer customer = customerService.getCustomerByUsername(principal.getName());
		model.addAttribute("customer", customer);
		model.addAttribute("subTotal", Double.parseDouble(String.format("%.2f", subTotal * 1)));
		model.addAttribute("tax", Double.parseDouble(String.format("%.2f", Util.getTax(subTotal))));
		model.addAttribute("total", Double.parseDouble(String.format("%.2f", subTotal + Util.getTax(subTotal))));

		return "viewCart";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/cart/checkout", method = RequestMethod.POST)
	public String checkOut(HttpServletRequest request, Order order, Address shippingAddress, Principal principal) {

		System.out.println(order.getSubtotal());
		String orderItemsSession = Util.getOrderItemSession(principal.getName());
		Collection<OrderItem> items = ((Map<Long, OrderItem>) request.getSession().getAttribute(orderItemsSession))
				.values();

		Customer customer = customerService.getCustomerByUsername(principal.getName());

		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderNumber = format.format(date) + (int) (Math.random() * 1000);

		order.setShippingAddress(shippingAddress);
		order.setDate(date);
		order.setNumber(orderNumber);
		order.setCustomer(customer);
		double orderSubtotal = 0;
		for (OrderItem item : items) {
			order.addItem(item);
			orderSubtotal += item.getAmount();
		}
		order.setSubtotal(Double.parseDouble(String.format("%.2f", orderSubtotal * 1)));
		order.setTax(Double.parseDouble(String.format("%.2f", Util.getTax(orderSubtotal))));
		order.setTotal(Double.parseDouble(String.format("%.2f", (orderSubtotal + Util.getTax(orderSubtotal)))));

		orderService.addOrder(order);

		request.getSession().removeAttribute(orderItemsSession);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Order successfully checked out");

		return "redirect:/customer/orders";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/cart/delete/{id}", method = RequestMethod.POST)
	public String deleteItem(@PathVariable long id, HttpServletRequest request, Principal principal) {
		String orderItemsSession = Util.getOrderItemSession(principal.getName());
		Map<Long, OrderItem> items = ((HashMap<Long, OrderItem>) request.getSession().getAttribute(orderItemsSession));
		items.remove(id);
		request.getSession().setAttribute(orderItemsSession, items);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Item successfully removed from cart");
		return "redirect:/cart";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/cart/edit/{id}", method = RequestMethod.POST)
	public String editItem(@PathVariable long id, HttpServletRequest request, Principal principal) {
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		String orderItemsSession = Util.getOrderItemSession(principal.getName());
		Map<Long, OrderItem> items = ((HashMap<Long, OrderItem>) request.getSession().getAttribute(orderItemsSession));
		OrderItem item = items.get(id);
		item.setQuantity(quantity);
		items.put(id, item);
		request.getSession().setAttribute(orderItemsSession, items);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Item quantity successfully updated to cart");
		return "redirect:/cart";
	}

}
