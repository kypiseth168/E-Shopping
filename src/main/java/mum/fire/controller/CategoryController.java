package mum.fire.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import mum.fire.domain.Category;
import mum.fire.service.CategoryService;
import mum.fire.utility.Util;

@Controller
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public String getAllCategories(Model model) {
		List<Category> categories = categoryService.getAllCategories();
		model.addAttribute("categoryList", categories);
		return "categories";
	}

	@RequestMapping(value = "/categories/edit/{id}", method = RequestMethod.POST)
	public String updateCategory(Category updatedCategory, @PathVariable int id, HttpServletRequest request) {
		categoryService.updateCategory(updatedCategory, id);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Category successfully updated");
		return "redirect:/categories";
	}

	@RequestMapping(value = "/categories/add", method = RequestMethod.POST)
	public String addCategory (Category category, BindingResult bindingResult ,HttpServletRequest request) {
		if(bindingResult.hasErrors()){
			return "redirect:/categories/add";
		}
		categoryService.addCategory(category);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Category successfully added");
		return "redirect:/categories";
	}

	@RequestMapping(value = "/categories/delete/{id}", method = RequestMethod.POST)
	public String deleteCategory(@PathVariable long id, HttpServletRequest request) {
		categoryService.deleteCategory(id);
		request.getSession().setAttribute(Util.SUCCESS_MESSAGE, "Category successfully deleted");
		return "redirect:/categories";
	}

}
