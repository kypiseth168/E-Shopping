package mum.fire.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	private long id;

	@Embedded
	private Address address;

	private String firstname;

	private String lastname;

	@Column(name = "is_active")
	private boolean isActive;

	@OneToOne(cascade= CascadeType.PERSIST)
	private User user;

	public Customer() {
	}

	public Customer(String firstname, String lastname, Address address, User user) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.user = user;
		this.isActive = true;
	}

	public long getId() {
		return id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
