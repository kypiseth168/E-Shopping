package mum.fire.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Order_table")
public class Order {

	@Id
	@GeneratedValue
	private long id;

	@Column(name = "order_number")
	private String number;

	@Column(name = "order_date")
	private Date date;
	
	private double total;		
	private double tax;
	private double subtotal;

	@Embedded
	private Address shippingAddress;

	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name="order_id")
	private List<OrderItem> items = new ArrayList<OrderItem>();

	@ManyToOne
	private Customer customer;

	public Order() {
		// TODO Auto-generated constructor stub
	}

	public Order(String number, Date date, Customer customer, Address shippingAddress) {
		super();
		this.number = number;
		this.date = date;
		this.customer = customer;
		this.shippingAddress = shippingAddress;
	}

	public void addItem(OrderItem orderItem) {
		items.add(orderItem);
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public long getId() {
		return id;
	}

	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public List<OrderItem> getItems() {
		return Collections.unmodifiableList(items);
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

}
