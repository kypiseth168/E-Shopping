package mum.fire.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class OrderItem {

	@Id
	@GeneratedValue
	private long id;

	@Column(name = "item_quantity")
	private int quantity;

	@ManyToOne
	private ProductHistory productHistory;
	
	//@Access(AccessType.PROPERTY)
	@Transient
	private double amount;

	public OrderItem() {
		// TODO Auto-generated constructor stub
	}

	public OrderItem(int quantity, ProductHistory productHistory) {
		super();
		this.quantity = quantity;
		this.productHistory = productHistory;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public ProductHistory getProductHistory() {
		return productHistory;
	}

	public void setProductHistory(ProductHistory productHistory) {
		this.productHistory = productHistory;
	}

	public long getId() {
		return id;
	}

	public double getAmount() {
		return quantity*productHistory.getPrice();
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
