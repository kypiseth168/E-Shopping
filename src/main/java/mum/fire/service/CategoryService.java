package mum.fire.service;

import java.util.List;

import mum.fire.domain.Category;

public interface CategoryService {

	public abstract List<Category> getAllCategories();

	public abstract Category updateCategory(Category updatedCategory, long id);

	public abstract Category deleteCategory(long id);

	public abstract Category addCategory(Category category);

	public abstract Category getCategory(long id);

}
