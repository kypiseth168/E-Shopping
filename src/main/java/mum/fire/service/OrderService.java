package mum.fire.service;

import java.util.List;

import mum.fire.domain.Order;

public interface OrderService {

	public List<Order> getAllOrders();

	public Order getOrder(long orderid);

	public Order addOrder(Order order);

	public List<Order> getOrderByCustomerId(long customerid);

}
