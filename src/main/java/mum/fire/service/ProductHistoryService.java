package mum.fire.service;

import mum.fire.domain.ProductHistory;

public interface ProductHistoryService {

	public abstract ProductHistory addProductHistory(ProductHistory productHistory);

	public abstract ProductHistory getLastProductHistoryByProductId(long id);

}
