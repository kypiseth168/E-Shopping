package mum.fire.service;

import mum.fire.domain.Customer;

public interface CustomerService {

	public Customer getCustomerByUsername(String username);

	Customer updateCustomer(Customer updatedCustomer, String username);

	void createCustomer(Customer c);

}
