package mum.fire.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mum.fire.domain.Category;
import mum.fire.domain.Product;
import mum.fire.domain.ProductHistory;
import mum.fire.repository.CategoryRepository;
import mum.fire.repository.query.impl.AllCateogries;
import mum.fire.service.CategoryService;
import mum.fire.service.ProductHistoryService;
import mum.fire.service.ProductService;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductHistoryService productHistoryService;

	@Override
	public List<Category> getAllCategories() {
		return categoryRepository.getList(AllCateogries.INSTANCE);
	}

	@Override
	public Category addCategory(Category category) {
		category.setActive(true);
		return categoryRepository.add(category);
	}

	@Override
	public Category deleteCategory(long id) {
		Category category = getCategory(id);
		category.setActive(false);
		List<Product> products = productService.getProductByCategory(id);
		for (Product product : products) {
			productService.deleteProduct(product);
		}
		return categoryRepository.update(category);
	}

	@Override
	public Category getCategory(long id) {
		return categoryRepository.get(id);
	}

	@Override
	public Category updateCategory(Category updatedCategory, long id) {
		Category category = getCategory(id);
		category.setName(updatedCategory.getName());
		List<Product> products = productService.getProductByCategory(id);
		for (Product product : products) {
			product.setCategory(category);
			ProductHistory productHistory = new ProductHistory(product);
			productHistoryService.addProductHistory(productHistory);
		}
		return categoryRepository.update(category);
	}

}
