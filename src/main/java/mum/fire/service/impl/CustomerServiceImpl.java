package mum.fire.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mum.fire.domain.Customer;
import mum.fire.repository.CustomerRepository;
import mum.fire.repository.query.impl.CustomerByUsername;
import mum.fire.service.CustomerService;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer getCustomerByUsername(String username) {
		return customerRepository.get(new CustomerByUsername(username));
	}

	@Override
	public Customer updateCustomer(Customer updatedCustomer, String username) {
		Customer customer = getCustomerByUsername(username);
		customer.setAddress(updatedCustomer.getAddress());
		customer.setFirstname(updatedCustomer.getFirstname());
		customer.setLastname(updatedCustomer.getLastname());
		customerRepository.update(customer);
		return customer;
	}

	@Override
	public void createCustomer(Customer c) {
		customerRepository.add(c);
	}
	
	

}
