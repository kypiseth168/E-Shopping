package mum.fire.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mum.fire.domain.ProductHistory;
import mum.fire.repository.ProductHistoryRepository;
import mum.fire.repository.query.impl.LatestProductHistoryByProductId;
import mum.fire.service.ProductHistoryService;

@Service
@Transactional
public class ProductHistoryServiceImpl implements ProductHistoryService {

	@Autowired
	private ProductHistoryRepository productHistoryRepository;

	@Override
	public ProductHistory addProductHistory(ProductHistory productHistory) {
		return productHistoryRepository.add(productHistory);
	}

	@Override
	public ProductHistory getLastProductHistoryByProductId(long id) {
		return productHistoryRepository.get(new LatestProductHistoryByProductId(id));
	}
	
	
	


}
