package mum.fire.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mum.fire.domain.Order;
import mum.fire.repository.OrderRepository;
import mum.fire.repository.query.impl.AllOrders;
import mum.fire.repository.query.impl.OrdersByCustomerId;
import mum.fire.service.OrderService;

@Service	
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public List<Order> getAllOrders() {
		return orderRepository.getList(AllOrders.INSTANCE);
	}

	@Override
	public Order getOrder(long orderId) {
		return orderRepository.get(orderId);
	}

	@Override
	public Order addOrder(Order order) {
		orderRepository.add(order);
		return order;
	}

	@Override
	public List<Order> getOrderByCustomerId(long customerid) {
		return orderRepository.getList(new OrdersByCustomerId(customerid));
	}

}
