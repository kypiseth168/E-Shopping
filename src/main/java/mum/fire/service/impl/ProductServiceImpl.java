package mum.fire.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mum.fire.domain.Product;
import mum.fire.domain.ProductHistory;
import mum.fire.repository.ProductRepository;
import mum.fire.repository.query.impl.AllProducts;
import mum.fire.repository.query.impl.ProductsByCategoryId;
import mum.fire.repository.query.impl.ProductsByCategoryIdAndName;
import mum.fire.service.ProductHistoryService;
import mum.fire.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductHistoryService productHistoryService;
	
	@Override
	public List<Product> getAllProducts() {
		return productRepository.getList(AllProducts.INSTANCE);
	}

	@Override
	public List<Product> getProductByCategory(long id) {
		return productRepository.getList(new ProductsByCategoryId(id));
	}

	@Override
	public Product getProductById(long id) {
		return productRepository.get(id);
	}

	@Override
	public Product updateProduct(Product updatedProduct,long id) {
		Product product = getProductById(id);
		product.setCategory(updatedProduct.getCategory());
		product.setCode(updatedProduct.getCode());
		product.setDescription(updatedProduct.getDescription());
		product.setName(updatedProduct.getName());
		product.setPrice(updatedProduct.getPrice());
		ProductHistory productHistory = new ProductHistory(product);
		productHistoryService.addProductHistory(productHistory);
		return productRepository.update(product);
	}

	@Override
	public Product addProduct(Product product) {
		product.setActive(true);
		ProductHistory productHistory = new ProductHistory(product);
		productHistoryService.addProductHistory(productHistory);
		return productRepository.add(product);
	}
	
	@Override
	public Product deleteProduct(long id) {
		Product product = getProductById(id);
		return deleteProduct(product);
	}
	
	@Override
	public Product deleteProduct(Product product) {
		product.setActive(false);
		return productRepository.update(product);
	}

	@Override
	public List<Product> findProduct(long categoryId, String productName) {
		return productRepository.getList(new ProductsByCategoryIdAndName(categoryId, productName));
	}

}
