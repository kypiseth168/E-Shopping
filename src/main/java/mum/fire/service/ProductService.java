package mum.fire.service;

import java.util.List;

import mum.fire.domain.Product;

public interface ProductService {

	public abstract List<Product> getAllProducts();

	public abstract List<Product> getProductByCategory(long id);

	public abstract List<Product> findProduct(long categoryId, String productName);

	public abstract Product getProductById(long id);

	public abstract Product updateProduct(Product product, long id);

	public abstract Product addProduct(Product product);

	public abstract Product deleteProduct(long id);

	public abstract Product deleteProduct(Product product);

}
